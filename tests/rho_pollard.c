#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "bignum.h"
#include "../src/list.h"

unsigned long int gcd(unsigned long int a, unsigned long int b){
  unsigned long int old_r, r = 1l;
  if(a < b)
    return gcd(b,a);

  while(r != 0l){
    old_r = r;
    r = a % b;
    a = b;
    b = r;
  }

  return old_r;
}

unsigned long int f(unsigned long int x) {
  return x*x + 1l;
}

unsigned long int Pollard(unsigned long int n) {
  list_t l = NULL;
  unsigned long int xj = 2l;
  unsigned long int * x = malloc(sizeof(unsigned long int));
  unsigned long int xi = f(xj) % n;
  *x = xi;
  list_push(l, &xj);
  list_t head = l;
  while(gcd(xi-xj, n) == 1l) {
    if (list_is_empty(l)) {
      l = list_push(head, x);
      head = l;
      x = malloc(sizeof(unsigned long int));
      xi = f(xi) % n;
      *x = xi;
      xj = (unsigned long int)l->val;
    }
    else {
      xj = (unsigned long int)l->val;
      l = l->next;
    }
  } 
  return gcd(xi-xj, n);
}

unsigned long int Floyd(unsigned long int n) {
  unsigned long int a = f(0) % n;
  unsigned long int b = f(a) % n;
  while(gcd(a-b, n) == 1l) {
    a = f(a) % n;
    b = f(f(b)) % n;
  }
  return gcd(a-b, n);
}

bignum_t fb(bignum_t x) { 
  return bignum_add(bignum_pow(x,bignum_from_int(2)), bignum_from_int(1));
}

bignum_t Floydb(bignum_t n) {
  bignum_t a = bignum_mod(fb(bignum_from_int(0)), n);
  bignum_t b = bignum_mod(fb(a), n);
  while(bignum_gcd(bignum_sub(a, b), n) == bignum_from_int(1)) {
    a = bignum_mod(fb(a), n);
    b = bignum_mod(fb(fb(b)), n);
  }
  return bignum_gcd(bignum_sub(a, b), n);
}

int main() {
  // En utilisant l'algorithme rho de Pollard, factorisez les entiers suivants

  unsigned long int n1 = 17l * 113l;
  unsigned long int n2 = 239l * 431l;
  unsigned long int n3 = 3469l * 4363l;
  unsigned long int n4 = 15241l * 18119l;
  unsigned long int n5 = 366127l * 416797l;
  unsigned long int n6 = 15651941l * 15485863l;
  unsigned long int c = 38724383153898042047;
  bignum_t n7, n8;
 
  n7 = bignum_mul(bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(127)),
                             bignum_from_int(1)),
                  bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(61)),
                             bignum_from_int(1)));

  n8 = bignum_mul(bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(607)),
                             bignum_from_int(1)),
                  bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(2203)),
                             bignum_from_int(1)));


  
  printf("PGCD(42,24) = %lu\n", gcd(42,24));
  printf("PGCD(42,24) = %s\n", bignum_to_str(bignum_gcd(bignum_from_int(42),bignum_from_int(24))));
  printf("Pollard(n2) = %lu\n", Pollard(c));
  printf("test Floyd(n1) = %lu\n", Floyd(n6));
  printf("Pollardb(n2) = %u\n", Floydb(bignum_from_int(3690695766483351530794676439830257543922141687210501147298354087480176401844018846326080203515780378747288947762155414725538652542694710470755247446196044993704878051067535052239069960334082000767874184719270641249303434947081025040239272271626071130379889976431988825473480465355179515911138748016040243725871108020442758061810439317209861207058234330711521433650953300967227905950465091408791621447463729290452294761822186055396133226591204262813637777415335931848460065535163154685911298342242364471330425392542091309251585436553045696568220587390047866616535536536241656030930150767404768048937784003401236422662145041050241722628337191279501628304062705552735290010126482161888234744507113744025291341554526295830961741253976619986741986166366265540931051723011080627487961011923392810991006511502026504124171996298017482737902169760424671836750405515667524697239733874537199321616535557054689980107)));
  
  return 0;
}
